const redisServerUrl = 'http://localhost:8080'

let ytSuggestions = []
let activeYtSuggestions = []
let selectedSuggestion = 0

let displayNotifications = false;


document.addEventListener('DOMContentLoaded', async () => {

    document.querySelectorAll('a').forEach( elem => {
        elem.addEventListener('click', ev => {
            // ev.preventDefault()
        })
    })

    document.querySelector('body').addEventListener('click', () => {
        displayNotifications = false;
        collapseOptions()
        renderNotifications()
    })

    document.querySelector('.notifications').addEventListener('click', ev => {
        ev.stopPropagation()
    })

    document.querySelector('.notifications-box').addEventListener('click', ev => {
        displayNotifications = ! displayNotifications
        ev.stopPropagation()
        renderNotifications()
    })

    document.querySelectorAll('.search input[type=button]').forEach( elem => {
        elem.addEventListener('click', () => { search(elem) })
    })

    document.querySelectorAll('.search input[type=text]').forEach( elem => {
        elem.addEventListener('keyup', (ev) => {
            if (ev.keyCode == 13) {
                search(ev.target.closest('.search').querySelector('input[type=button]'))
            }
            if (ev.keyCode == 38) {
                if (selectedSuggestion > 0) {
                    selectedSuggestion--
                    renderYtSuggestions()
                }
            }
            if (ev.keyCode == 40) {
                if (selectedSuggestion < activeYtSuggestions.length-1) {
                    selectedSuggestion++
                    renderYtSuggestions()
                }
            }
        })
    })

    document.querySelector('.search.youtube input[type=text]').addEventListener('blur', (ev) => {
        activeYtSuggestions = []
        renderYtSuggestions()
    })

    document.querySelector('.search.youtube input[type=text]').addEventListener('keyup', (ev) => {

        if ([38,40].includes(ev.keyCode)) {
            return
        }

        if (ev.target.value.length > 3) {
            selectedSuggestion = 0
            activeYtSuggestions = ytSuggestions.filter( sugg => sugg.includes(ev.target.value.toLowerCase()))
        }
        else {
            activeYtSuggestions = []
        }
        renderYtSuggestions()
    })
    
    updateNotificationsBell()

    function renderYtSuggestions() {
        const suggestionsElement = document.querySelector('.search.youtube .suggestions')
        
        if (activeYtSuggestions.length > 0) {
            suggestionsElement.innerHTML = ''
            activeYtSuggestions.forEach( (sugg, index) => {
                const suggElement = document.createElement('div')
                suggElement.classList.add('suggestion')
                if (index == selectedSuggestion) {
                    suggElement.classList.add('selected')
                }
                suggElement.innerHTML = sugg
                suggestionsElement.append(suggElement)
            })
            suggestionsElement.style.display = 'block'
        }
        else {
            suggestionsElement.style.display = 'none'
        }

    }
    initSuggestions()
    renderNotifications()
    setInterval( () => { window.location.reload() }, 1000 * 60 * 30)
})

async function initSuggestions() {

    let tasksResponse = await fetch(`${redisServerUrl}/getlist/tasks`)

    if (tasksResponse.ok) {
        let tasks = await tasksResponse.json()

        const tasksElement = document.querySelector('.tasks')
        tasksElement.innerHTML = ''

        tasks.forEach( task => {
            const taskDiv = document.createElement('div')
            taskDiv.innerHTML = `• ${task}`
            tasksElement.prepend(taskDiv)
        })

    }

    let ytSuggestionsResponse = await fetch(`${redisServerUrl}/getset/yt-suggestions`)

    if (ytSuggestionsResponse.ok) {
        ytSuggestions = (await ytSuggestionsResponse.json()).reverse()
    }

}

function search(button) {

    const shortName = {
        'youtube': 'yt',
        'wikipedia': 'wp',
    }

    let query = ''
    if (activeYtSuggestions.length > 0) {
        query = activeYtSuggestions[selectedSuggestion]
    }
    else {
        query = button.closest('.search').querySelector('input[type=text]').value
    }
    const formData = new FormData()
    formData.append('setName', `${shortName[button.dataset.name]}-suggestions`)
    formData.append('value', query.toLowerCase())

    fetch(`${redisServerUrl}/addtoset/`, {
        method : 'POST',
        body: formData,
    })

    let url
    switch (button.dataset.name) {
        case 'youtube': url     = `https://youtube.com/results?search_query=${query}`; break;
        case 'ddg': url         = `https://duckduckgo.com/?t=h_&q=${query}`; break;
        case 'wikipedia': url   = `https://wikipedia.org/w/index.php?go=Go&search=${query}`; break;
    }
    window.open(url, '_blank').focus()
}

function renderNotifications() {
    const notificationsList = document.querySelector('.notifications')
    notificationsList.innerHTML = ''

    if (displayNotifications) {
        notificationsList.classList.add('show')

        let activeNotifications = notifications.filter( n => {
            let notificationDate = new Date(n.activationDate.split(" ").join("T") + '.000Z')
            return notificationDate < now()
        })

        activeNotifications.forEach( notif => {
            const notificationElem = document.createElement('div')
            notificationElem.classList.add('notification')
            notificationElem.dataset.key = notif.key
            if (notif.unread) {
                notificationElem.classList.add('unread')
            }

            const textElem = document.createElement('div')
            textElem.classList.add('text')
            textElem.innerHTML = notif.text
            notificationElem.append(textElem)

            const threeDotsElem = newThreeDots()
            notificationElem.append(threeDotsElem)

            const optionsElem = document.createElement('div')
            optionsElem.classList.add('options')
            if (notif.displayOptions) {
                optionsElem.classList.add('show')
            }
            
            const markReadElem = document.createElement('div')
            markReadElem.classList.add('option')
            markReadElem.classList.add('markRead')
            markReadElem.innerHTML = 'Marcar como leída'

            const postponeElem = document.createElement('div')
            postponeElem.classList.add('option')
            postponeElem.classList.add('postpone')
            postponeElem.innerHTML = 'Posponer'

            optionsElem.append(markReadElem)
            optionsElem.append(postponeElem)

            notificationElem.append(threeDotsElem)
            notificationElem.append(optionsElem)

            notificationsList.append(notificationElem)
        })

        initNotifications()
    }
    else {
        notificationsList.classList.remove('show')
    }

}

function newThreeDots() {
    const threeDotsElem = document.createElement('div')
    threeDotsElem.classList.add('three-dots')

    for (let i = 0; i < 3; i++) {
        const dotElem = document.createElement('div')
        dotElem.classList.add('dot')
        threeDotsElem.append(dotElem)
    }

    return threeDotsElem
}

function initNotifications() {
    document.querySelectorAll('.notification').forEach (elem => {
        elem.addEventListener('click', ev => {
            collapseOptions()
            renderNotifications()
        })
    })
    document.querySelectorAll('.notification .three-dots').forEach (elem => {
        elem.addEventListener('click', ev => {
            let key = ev.target.closest('.notification').dataset.key
            notifications = notifications.map( n => {
                if (key == n.key) {
                    n.displayOptions = ! n.displayOptions
                }
                else {
                    n.displayOptions = false
                }
                return n
            })
            renderNotifications()
            ev.stopPropagation()
        })
    })
    document.querySelectorAll('.notification .markRead').forEach (elem => {
        elem.addEventListener('click', ev => {
            let key = ev.target.closest('.notification').dataset.key
            notifications = notifications.map( n => {
                if (key == n.key) {
                    n.unread = false
                }
                return n
            })
            renderNotifications()
            updateNotificationsBell()
        })
    })
}

function collapseOptions() {
    notifications = notifications.map( n => {
        n.displayOptions = false
        return n
    })
}

function updateNotificationsBell() {
    let unreadNotifications = notifications.filter( n => {
        let notificationDate = new Date(n.activationDate.split(" ").join("T") + '.000Z')
        return notificationDate < now() && n.unread == true
    }).length

    if (unreadNotifications > 0) {
        document.querySelector('.notifications-box .number-point').classList.add('show')
        document.querySelector('.notifications-box .number-point .inside').innerHTML = unreadNotifications
    }
    else {
        document.querySelector('.notifications-box .number-point').classList.remove('show')
    }
}

function now() {
    let date = new Date()
    return date.setHours(date.getHours()-3)
}
