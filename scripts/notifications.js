let notifications = [
    {
        key: 3,
        text: 'Escribir carta',
        unread: false,
        displayOptions: false,
        activationDate: "2024-01-21 17:05:00"
    },
    {
        key: 4,
        text: 'Visitar xadres',
        unread: false,
        displayOptions: false,
        activationDate: "2024-06-26 10:00:00"
    },
    {
        key: 5,
        text: 'Lavar ropa',
        unread: true,
        displayOptions: false,
        activationDate: "2024-07-22 12:00:00"
    },
    {
        key: 6,
        text: 'Acicalarse',
        unread: true,
        displayOptions: false,
        activationDate: "2024-07-27 09:00:00"
    },
    {
        key: 7,
        text: 'Juego de cat purple',
        unread: false,
        displayOptions: false,
        activationDate: "2024-02-03 18:00:00"
    },
    {
        key: 8,
        text: 'Médico',
        unread: true,
        displayOptions: false,
        activationDate: "2024-08-17 13:00:00"
    },
    {
        key: 9,
        text: 'Mantenimiento gatito',
        unread: true,
        displayOptions: false,
        activationDate: "2024-07-31 12:00:00"
    },
    {
        key: 10,
        text: 'Llevar auto al mecánico',
        unread: false,
        displayOptions: false,
        activationDate: "2024-03-19 19:00:00"
    },
    {
        key: 12,
        text: 'Antiparasitario Coco',
        unread: true,
        displayOptions: false,
        activationDate: "2024-07-31 12:00:00"
    },
]
